package net.etiterin.pokeraids

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.telegram.telegrambots.ApiContextInitializer
import org.telegram.telegrambots.meta.logging.BotLogger
import org.telegram.telegrambots.meta.logging.BotsFileHandler
import java.io.IOException
import java.util.logging.ConsoleHandler
import java.util.logging.Level

private const val LOGTAG = "MAIN"

@SpringBootApplication
class PokeraidsApplication

fun main(args: Array<String>) {
    initDebugLogging()
    initBotApiContext()
    SpringApplication.run(PokeraidsApplication::class.java, *args)
}

private fun initDebugLogging() {
    BotLogger.setLevel(Level.ALL)
    BotLogger.registerLogger(ConsoleHandler())

    try {
        BotLogger.registerLogger(BotsFileHandler())
    } catch (e: IOException) {
        BotLogger.severe(LOGTAG, e)
    }
}

private fun initBotApiContext() {
    try {
        ApiContextInitializer.init()
    } catch (e: Exception) {
        BotLogger.error(LOGTAG, e)
    }
}
