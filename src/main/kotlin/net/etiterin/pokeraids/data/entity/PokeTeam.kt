package net.etiterin.pokeraids.data.entity

enum class PokeTeam {
    VALOR,
    MYSTIC,
    INSTINCT
}