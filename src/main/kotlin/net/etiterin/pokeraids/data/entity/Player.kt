package net.etiterin.pokeraids.data.entity

data class Player(
        val name: String,
        val team: PokeTeam,
        val level: Int
)