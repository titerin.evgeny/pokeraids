package net.etiterin.pokeraids.data.entity

data class Location(
        val lat: Double,
        val lng: Double
)