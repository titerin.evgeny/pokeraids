package net.etiterin.pokeraids.data.entity

import java.util.*

data class Raid(
        val location: Location,
        val pokemon: String,
        val meetingTime: Date,
        val raidCreator: Player,
        val participants: ArrayList<Player>,
        var comment: String? = null
)