package net.etiterin.pokeraids.telegram.config

interface BotConfig {

    fun getBotToken(): String

    fun getBotName(): String
}