package net.etiterin.pokeraids.telegram

import net.etiterin.pokeraids.telegram.state.StateMapper
import net.etiterin.pokeraids.telegram.state.StateStorage
import net.etiterin.pokeraids.telegram.utils.Sender
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.objects.Message
import javax.inject.Inject

@Component
class UserMessageHandler(
        @Inject private val stateStorage: StateStorage,
        @Inject private val stateMapper: StateMapper
) : MessageHandler {

    override fun sendResponseForMessage(message: Message, send: Sender) {
        val userId = message.from.id
        val userState = stateStorage.getUserState(userId)
        val stateHandler = stateMapper.mapStateToHandler(userState)

        stateHandler.sendResponseForMessage(message, send)
    }

    override fun canHandleMessage(message: Message): Boolean {
        return message.hasText() || message.hasLocation()
    }
}