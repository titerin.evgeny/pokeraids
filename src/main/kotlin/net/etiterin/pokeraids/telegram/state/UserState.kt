package net.etiterin.pokeraids.telegram.state

enum class UserState(val value: Int) {

    DEFAULT(0),
    SHOWING_RAIDS(1);

    companion object {
        private val map = UserState.values().associateBy(UserState::value)

        fun fromInt(type: Int) = map[type]
    }
}