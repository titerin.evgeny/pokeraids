package net.etiterin.pokeraids.telegram.state

interface StateStorage {

    fun getUserState(userId: Int): UserState

    fun setUserState(userId: Int, state: UserState)
}