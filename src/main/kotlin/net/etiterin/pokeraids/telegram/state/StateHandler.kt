package net.etiterin.pokeraids.telegram.state

import net.etiterin.pokeraids.telegram.utils.Sender
import org.telegram.telegrambots.meta.api.objects.Message

interface StateHandler {

    fun sendResponseForMessage(message: Message, send: Sender)
}