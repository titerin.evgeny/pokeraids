package net.etiterin.pokeraids.telegram.state

import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class UserStateMapper : StateMapper {

    private val stateHandlerMap = ConcurrentHashMap<UserState, StateHandler>()

    init {
        initHandlersMap()
    }

    override fun mapStateToHandler(userState: UserState) = stateHandlerMap[userState]!!

    private fun initHandlersMap() {
        stateHandlerMap[UserState.DEFAULT] = MainMenuStateHandler()
    }
}