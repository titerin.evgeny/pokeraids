package net.etiterin.pokeraids.telegram.state

interface StateMapper {

    fun mapStateToHandler(userState: UserState): StateHandler
}