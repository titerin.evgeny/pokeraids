package net.etiterin.pokeraids.telegram.state

import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

@Component
class UserStateStorage : StateStorage {

    private val stateMap = ConcurrentHashMap<Int, UserState>()

    override fun getUserState(userId: Int) = stateMap.getOrDefault(userId, UserState.DEFAULT)

    override fun setUserState(userId: Int, state: UserState) {
        stateMap[userId] = state
    }
}