package net.etiterin.pokeraids.telegram.state

import net.etiterin.pokeraids.telegram.utils.Sender
import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton

@Component
class MainMenuStateHandler : StateHandler {

    override fun sendResponseForMessage(message: Message, send: Sender) {
        val message = SendMessage().apply {
            enableMarkdown(true)
            text = "Возможные действия:"
            replyMarkup = createKeyboardMarkup()
            chatId = message.chatId.toString()
            replyToMessageId = message.messageId
        }
        send(message)
    }

    private fun createKeyboardMarkup(): InlineKeyboardMarkup {
        val firstKeyRow = mutableListOf(
                InlineKeyboardButton("Создать рейд").apply { callbackData = "data1" }
        )
        val secondKeyRow = mutableListOf(
                InlineKeyboardButton("Ближайшие рейды").apply { callbackData = "data2" }
        )
        return InlineKeyboardMarkup().apply {
            keyboard = mutableListOf(firstKeyRow, secondKeyRow)
        }
    }
}
