package net.etiterin.pokeraids.telegram

import net.etiterin.pokeraids.telegram.config.BotConfig
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import javax.inject.Inject

@Component
class PokeRaidsBot(
        @Inject private val botConfig: BotConfig,
        @Inject private val messageHandler: MessageHandler
) : TelegramLongPollingBot() {

    override fun onUpdateReceived(update: Update) {
        if (update.hasMessage()) {
            val message = update.message
            handleUserMessage(message)
        }
    }

    private fun handleUserMessage(message: Message) {
        if (messageHandler.canHandleMessage(message)) {
            messageHandler.sendResponseForMessage(message) { response -> execute(response) }
        }
    }

    override fun getBotUsername() = botConfig.getBotName()

    override fun getBotToken() = botConfig.getBotToken()
}