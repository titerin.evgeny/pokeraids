package net.etiterin.pokeraids.telegram.utils

import org.telegram.telegrambots.meta.api.methods.BotApiMethod
import org.telegram.telegrambots.meta.api.objects.Message

typealias Sender = (BotApiMethod<Message>) -> Unit