package net.etiterin.pokeraids.telegram

import net.etiterin.pokeraids.telegram.utils.Sender
import org.telegram.telegrambots.meta.api.objects.Message

interface MessageHandler {

    fun sendResponseForMessage(message: Message, send: Sender)

    fun canHandleMessage(message: Message): Boolean
}
