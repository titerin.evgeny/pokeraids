package net.etiterin.pokeraids

import net.etiterin.pokeraids.telegram.PokeRaidsBot
import org.telegram.telegrambots.meta.TelegramBotsApi
import javax.annotation.PostConstruct
import javax.inject.Inject

class StartMonitor(@Inject private val pokeRaidsBot: PokeRaidsBot) {

    private lateinit var telegramBotsApi: TelegramBotsApi

    @PostConstruct
    private fun initApiAndRegisterBot() {
        telegramBotsApi = createLongPollingTelegramApi()
        telegramBotsApi.registerBot(pokeRaidsBot)
    }

    private fun createLongPollingTelegramApi() = TelegramBotsApi()
}