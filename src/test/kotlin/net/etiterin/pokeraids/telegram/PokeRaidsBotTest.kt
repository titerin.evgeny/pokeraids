package net.etiterin.pokeraids.telegram

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.nhaarman.mockitokotlin2.*
import net.etiterin.pokeraids.telegram.config.BotConfig
import net.etiterin.pokeraids.telegram.utils.Sender
import org.junit.Assert.assertEquals
import org.junit.Test
import org.telegram.telegrambots.meta.api.objects.Update

class PokeRaidsBotTest {

    private val botConfig: BotConfig = mock()
    private val messageHandler: MessageHandler = mock()
    private val send: Sender = mock()

    private val handler = PokeRaidsBot(botConfig, messageHandler)

    @Test
    fun `should handle incoming message`() {
        whenever(messageHandler.canHandleMessage(any())).thenReturn(true)
        val update = getUpdateWithMessage()

        handler.onUpdateReceived(update)

        verify(messageHandler).sendResponseForMessage(any(), any())
    }

    private fun getUpdateWithMessage(): Update {
        val objectMapper = ObjectMapper()
        val updateString = "{\"message\": {\"text\":\"test message\"}}"
        return objectMapper.readValue(updateString)
    }

    @Test
    fun `should not handle update without message`() {
        val update = getUpdateWithoutMessage()

        handler.onUpdateReceived(update)

        verify(send, never()).invoke(any())
    }

    private fun getUpdateWithoutMessage() = Update()

    @Test
    fun `should not try handle message if can't`() {
        whenever(messageHandler.canHandleMessage(any())).thenReturn(false)
        val update = getUpdateWithMessage()

        handler.onUpdateReceived(update)

        verify(send, never()).invoke(any())
    }

    @Test
    fun `should get bot name`() {
        val expectedBotName = "BotName"
        whenever(botConfig.getBotName()).thenReturn(expectedBotName)

        val actualBotName = handler.botUsername

        verify(botConfig).getBotName()
        assertEquals(actualBotName, expectedBotName)
    }

    @Test
    fun `should get bot token`() {
        val expectedBotToken = "BotToken"
        whenever(botConfig.getBotToken()).thenReturn(expectedBotToken)

        val actualBotToken = handler.botToken

        verify(botConfig).getBotToken()
        assertEquals(actualBotToken, expectedBotToken)
    }
}