package net.etiterin.pokeraids.telegram

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import net.etiterin.pokeraids.telegram.state.StateHandler
import net.etiterin.pokeraids.telegram.state.StateMapper
import net.etiterin.pokeraids.telegram.state.StateStorage
import net.etiterin.pokeraids.telegram.state.UserState
import net.etiterin.pokeraids.telegram.utils.Sender
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.telegram.telegrambots.meta.api.objects.Message

class UserMessageHandlerTest {

    private val stateStorage: StateStorage = mock()
    private val stateMapper: StateMapper = mock()
    private val stateHandler: StateHandler = mock()
    private val send: Sender = mock()

    private val messageHandler = UserMessageHandler(stateStorage, stateMapper)

    @Test
    fun `should handle messages with text`() {
        val message = createMessageWithText()

        val canHandleMessage = messageHandler.canHandleMessage(message)

        assertTrue(canHandleMessage)
    }

    private fun createMessageWithText() = createMessageFromJson(json = "{\"text\":\"test message\"}")

    private fun createMessageFromJson(json: String): Message {
        val objectMapper = ObjectMapper()
        return objectMapper.readValue(json)
    }

    @Test
    fun `should handle messages with location`() {
        val message = createMessageWithLocation()

        val canHandleMessage = messageHandler.canHandleMessage(message)

        assertTrue(canHandleMessage)
    }

    private fun createMessageWithLocation(): Message {
        return createMessageFromJson(json = "{\"location\":{\"longitude\": 0.0, \"latitude\": 0.0}}")
    }

    @Test
    fun `should not handle empty messages`() {
        val message = Message()

        val canHandleMessage = messageHandler.canHandleMessage(message)

        assertFalse(canHandleMessage)
    }

    @Test
    fun `should call storage for user state`() {
        presetMocks()
        val message = simpleUserMessage()

        messageHandler.sendResponseForMessage(message, send)

        verify(stateStorage).getUserState(userId = 123)
    }

    private fun presetMocks() {
        val userId = 123
        val userState = UserState.DEFAULT
        whenever(stateStorage.getUserState(userId)).thenReturn(userState)
        whenever(stateMapper.mapStateToHandler(userState)).thenReturn(stateHandler)
    }

    private fun simpleUserMessage(): Message {
        return createMessageFromJson(json = "{\"text\":\"test message\", \"from\":{\"id\":123}}")
    }

    @Test
    fun `should call state mapper for state handler`() {
        presetMocks()
        val message = simpleUserMessage()

        messageHandler.sendResponseForMessage(message, send)

        verify(stateMapper).mapStateToHandler(userState = UserState.DEFAULT)
    }
}