package net.etiterin.pokeraids.telegram.state

import org.junit.Assert.*
import org.junit.Test

class UserStateMapperTest {

    private val userStateMapper = UserStateMapper()

    @Test
    fun `should map default user state to main menu handler`() {
        val expectedStateHandlerClass = MainMenuStateHandler::class

        val actualStateHandlerClass = userStateMapper.mapStateToHandler(userState = UserState.DEFAULT)::class

        assertEquals(expectedStateHandlerClass, actualStateHandlerClass)
    }
}