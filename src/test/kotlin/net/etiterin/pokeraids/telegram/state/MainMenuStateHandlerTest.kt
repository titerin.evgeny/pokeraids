package net.etiterin.pokeraids.telegram.state

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import net.etiterin.pokeraids.telegram.utils.Sender
import org.junit.Test
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton

class MainMenuStateHandlerTest {

    private val send: Sender = mock()

    private val stateHandler = MainMenuStateHandler()

    @Test
    fun `should send proper response for user message`() {
        val message = simpleUserMessage()
        val expectedResponse = createExpectedResponse()

        stateHandler.sendResponseForMessage(message, send)

        verify(send).invoke(expectedResponse)
    }

    private fun simpleUserMessage(): Message {
        return createMessageFromJson(json = "{" +
                "\"text\":\"test message\", " +
                "\"message_id\":123, " +
                "\"chat\":" +
                "{\"id\":321}" +
                "}"
        )
    }

    private fun createMessageFromJson(json: String): Message {
        val objectMapper = ObjectMapper()
        return objectMapper.readValue(json)
    }

    private fun createExpectedResponse(): SendMessage {
        return SendMessage().apply {
            enableMarkdown(true)
            text = "Возможные действия:"
            replyMarkup = createKeyboardMarkup()
            chatId = "321"
            replyToMessageId = 123
        }
    }

    private fun createKeyboardMarkup(): InlineKeyboardMarkup {
        val firstKeyRow = mutableListOf(
                InlineKeyboardButton("Создать рейд").apply { callbackData = "data1" }
        )
        val secondKeyRow = mutableListOf(
                InlineKeyboardButton("Ближайшие рейды").apply { callbackData = "data2" }
        )
        return InlineKeyboardMarkup().apply {
            keyboard = mutableListOf(firstKeyRow, secondKeyRow)
        }
    }
}