package net.etiterin.pokeraids.telegram.state

import org.junit.Assert.assertEquals
import org.junit.Test

class UserStateStorageTest {

    private val stateStorage = UserStateStorage()

    @Test
    fun `should get default state if no state defined`() {
        val expectedState = UserState.DEFAULT

        val actualState = stateStorage.getUserState(userId = 123)

        assertEquals(actualState, expectedState)
    }

    @Test
    fun `should get previously set state`() {
        val expectedState = UserState.SHOWING_RAIDS
        stateStorage.setUserState(userId = 123, state = expectedState)

        val actualState = stateStorage.getUserState(userId = 123)

        assertEquals(actualState, expectedState)
    }
}